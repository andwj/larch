// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

//import "fmt"

type Scope struct {
	vars       []*LocalVar
	lookup     map[string]int
	parent     *Scope
	end_label  string
	skip_pcs   []int32
}

type LocalVar struct {
	name      string
	mutable   bool
	external  bool
	offset    int32
	scope     *Scope
	owner     *Closure
}

type Upvar struct {
	ptr *ArrayImpl
}

//----------------------------------------------------------------------

func (ctx *ParsingCtx) PushScope() *Scope {
	sc := new(Scope)

	sc.vars   = make([]*LocalVar, 0)
	sc.lookup = make(map[string]int)
	sc.parent = ctx.scope

	ctx.scope = sc
	return sc
}

func (ctx *ParsingCtx) PopScope() {
	if ctx.scope == nil {
		panic("PopScope with empty stack")
	}

	ctx.scope = ctx.scope.parent
}

func (ctx *ParsingCtx) AddLocal(name string, mutable bool) *LocalVar {
	if ctx.scope == nil {
		panic("AddLocal outside of a scope")
	}

	lvar := new(LocalVar)
	lvar.name    = name
	lvar.mutable = mutable
	lvar.scope   = ctx.scope
	lvar.owner   = ctx.fu

	// NOTE 1: var is marked external only if used in a lambda (as an upvar)

	// NOTE 2: offset is assigned during compiling phase

	idx := len(ctx.scope.vars)
	ctx.scope.vars = append(ctx.scope.vars, lvar)

	// the match-all name `_` is never stored in the lookup table
	if name != "_" {
		ctx.scope.lookup[name] = idx
	}
	return lvar
}

func (ctx *ParsingCtx) LookupLocal(name string) *LocalVar {
	for sc := ctx.scope; sc != nil; sc = sc.parent {
		idx, exist := sc.lookup[name]

		if exist {
			return sc.vars[idx]
		}
	}

	return nil  // not found
}

func (sc *Scope) HasLocal(name string) bool {
	_, exist := sc.lookup[name]
	return exist
}
