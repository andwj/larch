
Larch API
=========

Introduction
------------

This document outlines some thoughts on the API of the interpreter
when it is embedded into another piece of software.

System Facilities
-----------------

All functionality which deals with specifics of an operating-system will
need to be provided by the host software, including memory allocation,
input/output, and a few other things which are normally supplied by a
language's standard library (and not part of the language itself).

The interpreter must be inited with a pointer to a table of function
pointers, which provide the needed functionality.  Alternatively, the
interpreter could just link against a particular global name in the
host program.

Basic Usage
-----------

There will be an opaque data structure which represents a context for the
compilation and execution of code, let us call it `STATE`.

- `NewState()` creates a new STATE object, if possible.  it will be
  ready to use.

- `FreeState()` destroys a STATE object, and all resources (memory etc)
  which belongs to it.

- `LoadString()` attempts to parse a string containing code.  It returns a
  boolean to indicate success or failure.

- `LoadFile()` attempts to open a file and parse the code within.  It returns
  a boolean to indicate success or failure.

- `NextError()` removes the first unseen error message and returns it.  There
  may be multiple errors, so this should be called in a loop.

- `CompileCode` must be used after ALL the code files (or strings) have been
  parsed, and causes all functions, `do` blocks (etc) to be compiled.  This
  API function should not be called if there were any parsing errors.

- `NextDoBlock()` removes the first unseen top-level `do` block from the
  recently parsed and compiled code.  There may be multiple such blocks,
  so it should be called in a loop.

- `RunCode()` executes the code of a function or `do` block.  It returns a boolean
  to indicate success or failure.  On failure, the error message should be
  retrieved via the `NextError` API.

