// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "io"
import "fmt"
// import "strings"

// import "time"
// import "math/rand"
// import "path/filepath"

import "github.com/peterh/liner"

// the command-line editor for terminal input.
// always available in REPL, must be explicitly enabled in scripts.
var editor *liner.State

var ErrQuit   = fmt.Errorf("user quit")
var ErrAbort  = fmt.Errorf("user abort")
var ErrInput  = fmt.Errorf("input error")
var ErrCancel = fmt.Errorf("cancelled")

//----------------------------------------------------------------------

func RunREPL() (status int) {
	lar_AddARGS([]string{"REPL"})
	lar_EnterREPL()

	EnableEditor()

	// this will reset the terminal into a usable state should
	// a panic occur somewhere.
	// [ unfortunately there is no way to recover from serious
	//   runtime issues like overflowing the Go stack... ]
	defer editor.Close()

	for {
		err := ProcessUserInput()

		switch {
		case err == ErrQuit:
			return 0

		case err == ErrInput:
			return 1

		case err == ErrAbort:
			return 2
		}
	}
}

func EnableEditor() {
	if editor == nil {
		editor = liner.NewLiner()
		editor.SetCtrlCAborts(true)
	}
}

func ProcessUserInput() error {
	input, err := editor.Prompt("=> ")

	if err == io.EOF {
		fmt.Fprintf(os.Stderr, "\nEOF\n")
		return ErrQuit
	}
	if err == liner.ErrPromptAborted {
		fmt.Fprintf(os.Stderr, "Aborted\n")
		return ErrAbort
	}
	if err != nil {
		fmt.Fprintf(os.Stderr, "Input Error: %s\n", err.Error())
		return ErrInput
	}

	// ignore blank lines
	if input == "" {
		return nil
	}

	// see if user wants to quit
	switch input {
	case "quit", "QUIT", "exit", "EXIT":
		return ErrQuit
	}

	var kbd KeyboardReader
	kbd.Begin(input)

	// the REPL can recover from compile/runtime errors...

	load_err := lar_LoadLine(&kbd)

	// must do this after LoadLine, in order to capture any additional
	// input lines which may have been requested.
	editor.AppendHistory(kbd.JoinLines())

	if load_err == OK {
		if lar_CompileCode() == OK {
			if ExecuteStatements(true) == OK {
				return nil  // ok
			}
		}
	}

	ShowErrors()
	lar_ClearErrors()

	// remove any pending statements
	for lar_NextStatement() != nil {
	}

	return nil  // ok
}

//----------------------------------------------------------------------

type KeyboardReader struct {
	// current data that has not been read yet
	buffer []byte

	// all the raw input lines
	lines []string
}

func (kbd *KeyboardReader) Begin(s string) {
	kbd.lines = make([]string, 1)
	kbd.lines[0] = s

	// need a newline!
	s = s + "\n"

	kbd.buffer = []byte(s)
}

func (kbd *KeyboardReader) Read(p []byte) (n int, err error) {
	if len(p) == 0 {
		return 0, nil
	}

	for len(kbd.buffer) == 0 {
		s, err := editor.Prompt(".. ")

		if err == io.EOF {
			// CTRL-D on a continuation line is a cancel
			return 0, ErrCancel
		}

		if err == liner.ErrPromptAborted {
			return 0, ErrAbort
		}

		if err != nil {
			return 0, err
		}

		kbd.lines = append(kbd.lines, s)

		// need a newline!
		s = s + "\n"

		kbd.buffer = []byte(s)
	}

	use_len := len(kbd.buffer)
	if use_len > len(p) {
		use_len = len(p)
	}

	copy(p, kbd.buffer[0:use_len])

	kbd.buffer = kbd.buffer[use_len:]

	return use_len, nil  // ok
}

func (kbd *KeyboardReader) JoinLines() string {
	// we separate each line with a space (NOT a newline).
	// while the liner package has a "Multi Line Mode", it seems
	// quite broken, so this is the best we can do...

	s := ""

	for idx, line := range kbd.lines {
		if idx > 0 {
			s = s + " "
		}
		s = s + line
	}
	return s
}

