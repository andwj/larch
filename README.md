
Larch README
============

by Andrew Apted, 2022.


About
-----

This language has a block-based syntax using `{}` curly brackets,
with lexical scope, and supports first-class functions, closures and
tail calls.  It is dynamically-typed.
It implements a REPL, but can also run code from one or more script files.


Status
------

Very broken right now!

There are two non-trivial sample programs: a small adventure
game and Conway's game of life, which are working fine in this
interpreter.


Documentation
-------------

TODO


Legalese
--------

Larch is under a permissive MIT license.

Larch comes with NO WARRANTY of any kind, express or implied.

See the [LICENSE.md](LICENSE.md) doc for the full terms.

