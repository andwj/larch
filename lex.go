// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "io"
import "fmt"
import "strings"
import "strconv"
import "unicode"
import "unicode/utf8"

// Node represents an element of a parsed line or file.
type Node struct {
	kind     NodeKind
	children []*Node
	str      string
	pos      Position

	val      *Value     // NX_Value, NX_Const, NX_Lambda
	tdef     *TypeDef   // ND_Map
	scope    *Scope     // NG_Block
	lvar     *LocalVar  // NX_Local, NS_Let
}

type NodeKind int

const (
	/* low level nodes */

	NL_ERROR NodeKind = iota    // str is the message
	NL_EOF
	NL_EOLN

	NL_Integer  // an int:   [-]DDDDDDD          or [-]0xHHHHHH
	NL_Float    // a float:  [-]DDD.DDD[e[+-]DD] or [-]0xHH.HHHp[+-]DDD
	NL_FltSpec  // a special floating-point constant e.g. "+INF"
	NL_Char     // a char literal in '', encoded as integer: DDDDD
	NL_Bool     // a boolean literal, encoded as integer 0 or 1
	NL_String   // a string in "", encoded as UTF-8
	NL_Nil      // the 'NIL' void constant

	NL_Name     // an identifier
	NL_Symbol   // a special symbol like `:` or `[`

	/* grouping nodes */

	NG_Line     // a parsed line of tokens
	NG_Expr     // a group of elements in `()` parentheses
	NG_Data     // a group of elements in `[]` square brackets
	NG_Block    // a group of elements in `{}` curly brackets
)

type Position struct {
	line int  // 0 if unknown
	file int  // 0 if unknown, index into all_filenames (+ 1)
}

type Lexer struct {
	reader   io.Reader
	pos      Position

	line    []rune  // original line, only NIL at start
	r       []rune  // current parse position in line, NIL when done

	pending  *Node  // a pending error from SkipAhead
	hit_eof  bool   // we have reached the end-of-file
}

// NewLexer creates a Lexer from a Reader (e.g. a file).
func NewLexer(r io.Reader, file int) *Lexer {
	lexer := new(Lexer)

	lexer.reader   = r
	lexer.pos.file = file
	lexer.hit_eof  = false

	return lexer
}

// Scan reads and parses the current file and returns the next high-level
// token, usually a NG_Line.  It returns a NL_ERROR if there was a parsing
// error, or NL_EOF when the end of the file is reached.
func (lex *Lexer) Scan() *Node {
	if lex.pending != nil {
		res := lex.pending
		lex.pending = nil
		return res
	}

	if lex.hit_eof  {
		return NewNode(NL_EOF, "", lex.pos)
	}

	return lex.scanGroup(NG_Line, "", "", false)
}

// MoreLines returns true if the next call to Scan will return a line
// *without* fetching another raw line from the io.Reader.  Note that
// the future line could be empty.
func (lex *Lexer) MoreLines() bool {
	if lex.pending != nil {
		return false
	}
	if lex.line == nil || lex.r == nil {
		return false
	}
	return true
}

// SkipAhead may be called after an error is returned from Scan(), and
// will reposition the token stream at the next top-level directive.
// If any error occurs, it will be the returned at next call to Scan().
func (lex *Lexer) SkipAhead() {
	lex.pending = nil

	for {
		err := lex.fetchNonCommentLine()

		if err != nil {
			if err.kind == NL_ERROR {
				lex.pending = err
			}
			return
		}

		s := ""
		r := lex.r

		for len(r) > 0 {
			ch := r[0]

			if ! LEX_IdentifierChar(ch) {
				break
			}

			s = s + string(ch)
			r = r[1:]
		}

		if LEX_CommonDirective(s) {
			// ok, stop here
			return
		}
	}
}

func (lex *Lexer) scanGroup(kind NodeKind, opener, closer string, in_block bool) *Node {
	group := NewNode(kind, "", lex.pos)

	for {
		tok := lex.nextToken()

		// handle an opening parenthesis or bracket
		switch {
		case tok.Match("("): tok = lex.scanGroup(NG_Expr, "(", ")", false)
		case tok.Match("["): tok = lex.scanGroup(NG_Data, "[", "]", false)
		case tok.Match("{"): tok = lex.scanBlock()
		}

		if tok.kind == NL_ERROR {
			return tok
		}

		if tok.kind == NL_EOF {
			if opener != "" {
				// use the starting line for this error message
				msg := "unterminated " + opener + closer + " group"
				return NewNode(NL_ERROR, msg, group.pos)
			}
			return tok
		}

		if tok.kind == NL_EOLN {
			// ignore it inside () and []
			if kind != NG_Line {
				continue
			}

			// handle '\' symbol to extend lines
			if group.Len() > 0 {
				last := group.children[group.Len() - 1]

				if last.Match("\\") {
					group.PopTail()
					continue
				}
			}

			return group
		}

		// found terminator of a group?
		if closer != "" {
			if tok.Match(closer) {
				return group
			}
		}
		if in_block && tok.Match("}") {
			lex.pending = group
			return tok
		}

		// detect a stray closing parenthesis or bracket
		if tok.Match(")") || tok.Match("]") || tok.Match("}") {
			msg := "stray " + tok.str + " found"
			return NewNode(NL_ERROR, msg, group.pos)
		}

		// a comma splits a physical line into two logical lines
		if tok.Match(",") {
			if kind != NG_Line || group.Len() == 0 {
				return NewNode(NL_ERROR, "stray , found", tok.pos)
			}

			return group
		}

		// check for misuse of the '\' symbol
		if kind == NG_Line {
			if group.Len() > 0 {
				last := group.children[group.Len() - 1]
				if last.Match("\\") {
					return NewNode(NL_ERROR, "can only use \\ at end of a line", tok.pos)
				}
			}
		} else {
			if tok.Match("\\") {
				return NewNode(NL_ERROR, "stray \\ found", tok.pos)
			}
		}

		// ensure non-empty lines begin at first token
		if kind == NG_Line {
			if group.Len() == 0 {
				group.pos = tok.pos
			}
		}

		group.Add(tok)
	}
}

func (lex *Lexer) scanBlock() *Node {
	group := NewNode(NG_Block, "", lex.pos)

	for {
		line := lex.scanGroup(NG_Line, "", "", true /* in_block */)

		if line.kind == NL_EOF {
			// use the starting line for this error message
			return NewNode(NL_ERROR, "unterminated {} block", group.pos)
		}
		if line.kind == NL_ERROR {
			return line
		}

		is_end := false

		if line.Match("}") {
			is_end = true
			line   = lex.pending
			lex.pending = nil
		}

		// do not add empty lines to a block
		if line.Len() > 0 {
			group.Add(line)
		}

		if is_end {
			return group
		}
	}
}

//----------------------------------------------------------------------

// nextToken parses the next low-level token from the file.
// Result can be NL_ERROR for a parsing problem, or NL_EOF at the
// end of the file, or NL_EOLN for the end of a line.
func (lex *Lexer) nextToken() *Node {
	for {
		// need a fresh line?
		if lex.line == nil || lex.r == nil {
			err := lex.fetchNonCommentLine()

			if err != nil {
				return err
			}
		} else {
			lex.skipWhitespace()
		}

		// reached end of the line?
		is_end := false
		if len(lex.r) == 0 {
			is_end = true
		} else if lex.r[0] == ';' {
			is_end = true
		}

		if is_end {
			lex.r = nil
			return NewNode(NL_EOLN, "", lex.pos)
		}

		// string ?
		if lex.r[0] == '"' {
			return lex.scanString()
		}

		// character literal ?
		if lex.r[0] == '\'' {
			return lex.scanCharacter()
		}

		// number ?
		if LEX_Number(lex.r) {
			return lex.scanNumber()
		}

		// handle '::' symbol
		if len(lex.r) >= 2 {
			if lex.r[0] == ':' && lex.r[1] == ':' {
				tok := NewNode(NL_Symbol, string(lex.r[0:2]), lex.pos)
				lex.r = lex.r[2:]
				return tok
			}
		}

		// handle non-ident symbols
		if strings.ContainsRune("()[]{}:,^", lex.r[0]) {
			tok := NewNode(NL_Symbol, string(lex.r[0]), lex.pos)
			lex.r = lex.r[1:]
			return tok
		}

		// anything else MUST be a name
		if LEX_IdentifierChar(lex.r[0]) {
			return lex.scanIdentifier()
		}

		msg := fmt.Sprintf("stray U+%04X in code", int(lex.r[0]))
		return NewNode(NL_ERROR, msg, lex.pos)
	}
}

func (lex *Lexer) scanIdentifier() *Node {
	pos := 0

	for pos < len(lex.r) {
		if ! LEX_IdentifierChar(lex.r[pos]) {
			break
		}
		pos += 1
	}

	s := string(lex.r[0:pos])
	lex.r = lex.r[pos:]

	// a hard-coded constant?
	switch s {
	case "NIL":
		nd := NewNode(NL_Nil, "", lex.pos)
		return nd

	case "FALSE":
		nd := NewNode(NL_Bool, "0", lex.pos)
		return nd

	case "TRUE":
		nd := NewNode(NL_Bool, "1", lex.pos)
		return nd

	case "+INF", "-INF", "NAN":
		return NewNode(NL_FltSpec, s, lex.pos)
	}

	return NewNode(NL_Name, s, lex.pos)
}

func (lex *Lexer) scanString() *Node {
	// skip leading quote
	lex.r = lex.r[1:]

	s := ""

	for {
		if len(lex.r) == 0 {
			return NewNode(NL_ERROR, "unterminated string", lex.pos)
		}

		ch := lex.r[0]

		// found closing quote?
		if ch == '"' {
			lex.r = lex.r[1:]
			break
		}

		// handle escape sequences
		if ch == '\\' {
			tok := lex.scanEscape()
			if tok.kind == NL_ERROR {
				return tok
			}
			s = s + tok.str
			continue
		}

		s = s + string(ch)
		lex.r = lex.r[1:]
	}

	return NewNode(NL_String, s, lex.pos)
}

func (lex *Lexer) scanCharacter() *Node {
	// skip leading quote
	lex.r = lex.r[1:]

	if len(lex.r) > 0 && lex.r[0] == '\'' {
		return NewNode(NL_ERROR, "empty char literal", lex.pos)
	}

	var ch rune

	// handle escapes
	if len(lex.r) >= 1 && lex.r[0] == '\\' {
		tok := lex.scanEscape()
		if tok.kind == NL_ERROR {
			return tok
		}

		if len(lex.r) < 1 || lex.r[0] != '\'' {
			return NewNode(NL_ERROR, "missing quote for char literal", lex.pos)
		}
		lex.r = lex.r[1:]

		r := []rune(tok.str)
		if !utf8.ValidString(tok.str) || len(r) != 1 {
			return NewNode(NL_ERROR, "invalid unicode char literal", lex.pos)
		}

		ch = r[0]

	} else {
		if len(lex.r) < 2 || lex.r[1] != '\'' {
			return NewNode(NL_ERROR, "missing quote for char literal", lex.pos)
		}
		ch    = lex.r[0]
		lex.r = lex.r[2:]
	}

	int_val := strconv.Itoa(int(ch))

	return NewNode(NL_Char, int_val, lex.pos)
}

// scanEscape handles anything after a `\` backquote.
// returns a NL_String node on success, or a NL_ERROR on failure.
func (lex *Lexer) scanEscape() *Node {
	// skip the backslash
	lex.r = lex.r[1:]

	if len(lex.r) == 0 {
		return NewNode(NL_ERROR, "missing char after \\ escape", lex.pos)
	}

	esc := NewNode(NL_String, "", lex.pos)
	ch  := lex.r[0]

	// octal?
	// requires one to three octal digits, as per C99 and NASM.
	if '0' <= ch && ch <= '7' {
		return lex.scanOctalEscape()
	}

	// hexadecimal?
	// requires one or two hexadecimal digits, as per C99 and NASM.
	if ch == 'x' {
		lex.r = lex.r[1:]
		return lex.scanHexEscape()
	}

	// unicode?
	// this follows the Rust syntax with hex digits in `{}`.
	if ch == 'u' {
		lex.r = lex.r[1:]
		return lex.scanUnicodeEscape()
	}

	// anything else will be a single character
	lex.r = lex.r[1:]

	switch ch {
	case '"', '`', '\'', '\\', '?':
		esc.str = string(ch)

	case 'a': esc.str = "\u0007"  // bell
	case 'b': esc.str = "\u0008"  // backspace
	case 't': esc.str = "\u0009"  // horizontal tab
	case 'n': esc.str = "\u000A"  // linefeed
	case 'v': esc.str = "\u000B"  // vertical tab
	case 'f': esc.str = "\u000C"  // formfeed
	case 'r': esc.str = "\u000D"  // carriage return
	case 'e': esc.str = "\u001B"  // escape

	default:
		if unicode.IsLetter(ch) {
			msg := "unknown escape \\" + string(ch)
			return NewNode(NL_ERROR, msg, lex.pos)
		} else {
			return NewNode(NL_ERROR, "bad escape sequence", lex.pos)
		}
	}

	return esc
}

func (lex *Lexer) scanOctalEscape() *Node {
	value := int(lex.r[0] - '0')
	lex.r  = lex.r[1:]

	if len(lex.r) > 0 && LEX_OctalDigit(lex.r[0]) {
		value = value * 8 + int(lex.r[0] - '0')
		lex.r = lex.r[1:]
	}
	if len(lex.r) > 0 && LEX_OctalDigit(lex.r[0]) {
		value = value * 8 + int(lex.r[0] - '0')
		lex.r = lex.r[1:]
	}

	if value > 0xFF {
		return NewNode(NL_ERROR, "illegal octal escape", lex.pos)
	}

	// convert to a string.
	// NOTE: this may be produce invalid UTF-8 !
	var b [1]byte
	b[0] = byte(value)

	return NewNode(NL_String, string(b[:]), lex.pos)
}

func (lex *Lexer) scanHexEscape() *Node {
	s := ""

	if len(lex.r) > 0 && LEX_HexDigit(lex.r[0]) {
		s = s + string(lex.r[0])
		lex.r = lex.r[1:]
	}
	if len(lex.r) > 0 && LEX_HexDigit(lex.r[0]) {
		s = s + string(lex.r[0])
		lex.r = lex.r[1:]
	}

	if s == "" {
		return NewNode(NL_ERROR, "missing digits in hex escape", lex.pos)
	}

	value, err := strconv.ParseUint(s, 16, 64)
	if err != nil {
		return NewNode(NL_ERROR, "illegal hex escape", lex.pos)
	}

	// convert to a string.
	// NOTE: this may be produce invalid UTF-8 !
	var b [1]byte
	b[0] = byte(value)

	return NewNode(NL_String, string(b[:]), lex.pos)
}

func (lex *Lexer) scanUnicodeEscape() *Node {
	if len(lex.r) == 0 {
		return NewNode(NL_ERROR, "missing { in unicode escape", lex.pos)
	}
	if lex.r[0] != '{' {
		return NewNode(NL_ERROR, "missing { in unicode escape", lex.pos)
	}

	lex.r = lex.r[1:]

	s := ""

	for {
		if len(lex.r) == 0 {
			return NewNode(NL_ERROR, "missing } in unicode escape", lex.pos)
		}

		ch := lex.r[0]

		if ch == '}' {
			lex.r = lex.r[1:]
			break
		}

		if LEX_HexDigit(ch) {
			// ok
		} else if unicode.IsLetter(ch) {
			return NewNode(NL_ERROR, "bad hex digit in unicode escape", lex.pos)
		} else {
			return NewNode(NL_ERROR, "missing } in unicode escape", lex.pos)
		}

		s = s + string(ch)
		lex.r = lex.r[1:]
	}

	if s == "" {
		return NewNode(NL_ERROR, "missing digits in unicode escape", lex.pos)
	}

	value, err := strconv.ParseUint(s, 16, 64)
	if err != nil || value > 0x10FFFF {
		return NewNode(NL_ERROR, "illegal unicode code point", lex.pos)
	}

	return NewNode(NL_String, string(rune(value)), lex.pos)
}

func (lex *Lexer) scanNumber() *Node {
	r := lex.r

	if len(r) >= 2 && r[0] == '0' && r[1] == 'b' {
		return lex.scanBinaryNumber(2)
	}
	if len(r) >= 3 && (r[0] == '-' || r[0] == '+') && r[1] == '0' && r[2] == 'b' {
		return lex.scanBinaryNumber(3)
	}

	if len(r) >= 2 && r[0] == '0' && r[1] == 'o' {
		return lex.scanOctalNumber(2)
	}
	if len(r) >= 3 && (r[0] == '-' || r[0] == '+') && r[1] == '0' && r[2] == 'o' {
		return lex.scanOctalNumber(3)
	}

	if len(r) >= 2 && r[0] == '0' && r[1] == 'x' {
		return lex.scanHexNumber(2)
	}
	if len(r) >= 3 && (r[0] == '-' || r[0] == '+') && r[1] == '0' && r[2] == 'x' {
		return lex.scanHexNumber(3)
	}

	return lex.scanDecimalNumber()
}

func (lex *Lexer) scanBinaryNumber(pos int) *Node {
	is_neg := (lex.r[0] == '-')

	lex.r = lex.r[pos:]

	s := ""

	for len(lex.r) > 0 {
		ch := lex.r[0]

		if ch == '0' || ch == '1' {
			s += string(ch)
			lex.r = lex.r[1:]
			continue
		}

		if unicode.IsDigit(ch) || unicode.IsLetter(ch) ||
			ch == '-' || ch == '+' || ch == '.' {

			msg := "illegal binary digit: " + string(ch)
			return NewNode(NL_ERROR, msg, lex.pos)
		}

		// all other symbols terminate the number
		break
	}

	if s == "" {
		return NewNode(NL_ERROR, "bad binary number (no digits)", lex.pos)
	}

	// convert to hexadecimal
	val, err := strconv.ParseUint(s, 2, 64)
	if err != nil {
		return NewNode(NL_ERROR, "bad binary number (too large)", lex.pos)
	}

	hex := "0x" + strconv.FormatUint(val, 16)
	if is_neg {
		hex = "-" + hex
	}
	return NewNode(NL_Integer, hex, lex.pos)
}

func (lex *Lexer) scanOctalNumber(pos int) *Node {
	is_neg := (lex.r[0] == '-')

	lex.r = lex.r[pos:]

	s := ""

	for len(lex.r) > 0 {
		ch := lex.r[0]

		if LEX_OctalDigit(ch) {
			s += string(ch)
			lex.r = lex.r[1:]
			continue
		}

		if unicode.IsDigit(ch) || unicode.IsLetter(ch) ||
			ch == '-' || ch == '+' || ch == '.' {

			msg := "illegal octal digit: " + string(ch)
			return NewNode(NL_ERROR, msg, lex.pos)
		}

		// all other symbols terminate the number
		break
	}

	if s == "" {
		return NewNode(NL_ERROR, "bad octal number (no digits)", lex.pos)
	}

	// convert to hexadecimal
	val, err := strconv.ParseUint(s, 8, 64)
	if err != nil {
		return NewNode(NL_ERROR, "bad octal number (too large)", lex.pos)
	}

	hex := "0x" + strconv.FormatUint(val, 16)
	if is_neg {
		hex = "-" + hex
	}
	return NewNode(NL_Integer, hex, lex.pos)
}

func (lex *Lexer) scanDecimalNumber() *Node {
	is_neg := (lex.r[0] == '-')

	if (lex.r[0] == '-' || lex.r[0] == '+') {
		lex.r = lex.r[1:]
	}

	has_digit := false
	has_dot   := false
	has_E     := false
	has_exp   := false

	s := ""

	for len(lex.r) > 0 {
		ch := lex.r[0]

		if unicode.IsDigit(ch) {
			if has_E {
				has_exp = true
			} else {
				has_digit = true
			}
			s += string(ch)
			lex.r = lex.r[1:]
			continue
		}

		if ch == '.' {
			if !has_digit || has_dot || has_E {
				return NewNode(NL_ERROR, "bad float (misplaced '.')", lex.pos)
			}
			has_dot = true
			s += string(ch)
			lex.r = lex.r[1:]
			continue
		}

		if ch == 'e' || ch == 'E' {
			if !has_digit || has_E {
				return NewNode(NL_ERROR, "bad float (misplaced 'e')", lex.pos)
			}
			has_E = true
			s += string(ch)
			lex.r = lex.r[1:]

			if len(lex.r) > 0 {
				if lex.r[0] == '-' || lex.r[0] == '+' {
					s += string(lex.r[0])
					lex.r = lex.r[1:]
				}
			}
			continue
		}

		if unicode.IsLetter(ch) || ch == '-' || ch == '+' {
			msg := "illegal decimal digit: " + string(ch)
			return NewNode(NL_ERROR, msg, lex.pos)
		}

		// all other symbols terminate the number
		break
	}

	if s == "" {
		return NewNode(NL_ERROR, "bad number (no digits)", lex.pos)
	}
	if has_E && !has_exp {
		return NewNode(NL_ERROR, "bad float (missing exponent)", lex.pos)
	}

	if is_neg {
		s = "-" + s
	}

	kind := NL_Integer
	if has_dot || has_exp {
		kind = NL_Float
	}
	return NewNode(kind, s, lex.pos)
}

func (lex *Lexer) scanHexNumber(pos int) *Node {
	is_neg := (lex.r[0] == '-')

	lex.r = lex.r[pos:]

	has_digit := false
	has_dot   := false
	has_P     := false
	has_exp   := false

	s := ""

	for len(lex.r) > 0 {
		ch := lex.r[0]

		if has_P && unicode.IsLetter(ch) {
			msg := "illegal hex exponent digit: " + string(ch)
			return NewNode(NL_ERROR, msg, lex.pos)
		}

		if unicode.Is(unicode.Hex_Digit, ch) {
			if has_P {
				has_exp = true
			} else {
				has_digit = true
			}
			s += string(ch)
			lex.r = lex.r[1:]
			continue
		}

		if ch == '.' {
			if !has_digit || has_dot || has_P {
				return NewNode(NL_ERROR, "bad hex float (misplaced '.')", lex.pos)
			}
			has_dot = true
			s += string(ch)
			lex.r = lex.r[1:]
			continue
		}

		if ch == 'p' || ch == 'P' {
			if !has_digit || has_P {
				return NewNode(NL_ERROR, "bad hex float (misplaced 'p')", lex.pos)
			}
			has_P = true
			s += string(ch)
			lex.r = lex.r[1:]

			if len(lex.r) > 0 {
				if lex.r[0] == '-' || lex.r[0] == '+' {
					s += string(lex.r[0])
					lex.r = lex.r[1:]
				}
			}
			continue
		}

		if unicode.IsLetter(ch) || ch == '-' || ch == '+' {
			msg := "illegal hex digit: " + string(ch)
			return NewNode(NL_ERROR, msg, lex.pos)
		}

		// all other symbols terminate the number
		break
	}

	if s == "" {
		return NewNode(NL_ERROR, "bad hex number (no digits)", lex.pos)
	}
	if (has_dot && !has_P) || (has_P && !has_exp) {
		return NewNode(NL_ERROR, "bad hex float (missing exponent)", lex.pos)
	}

	s = "0x" + s
	if is_neg {
		s = "-" + s
	}

	kind := NL_Integer
	if has_dot || has_exp {
		kind = NL_Float
	}
	return NewNode(kind, s, lex.pos)
}

//----------------------------------------------------------------------

func (lex *Lexer) fetchNonCommentLine() *Node {
	for {
		err := lex.fetchNormalLine()

		if err != nil {
			return err
		}

		// ignore `#!` to allow using as a Unix script interpreter
		if len(lex.r) >= 2 {
			if lex.r[0] == '#' && lex.r[1] == '!' {
				continue
			}
		}

		// ignore line comments
		if len(lex.r) >= 1 {
			if lex.r[0] == ';' {
				continue
			}
		}

		return nil  // ok
	}
}

func (lex *Lexer) fetchNormalLine() *Node {
	// this handles multi-line comments, like: `#[[` and `#]]`

	cur_depth  := 0
	start_line := 0

	for {
		err := lex.fetchRawLine()

		if err == nil {
			// ok
		} else {
			if err.kind == NL_EOF && cur_depth > 0 {
				pos := lex.pos
				pos.line = start_line
				return NewNode(NL_ERROR, "unterminated comments", pos)
			}
			return err
		}

		lex.skipWhitespace()

		opener := LEX_MultilineComment(lex.r, '[')
		closer := LEX_MultilineComment(lex.r, ']')

		if opener > 0 {
			if cur_depth == 0 {
				cur_depth  = opener
				start_line = lex.pos.line
			} else if opener < cur_depth {
				// ignore a lower depth
			} else {
				return NewNode(NL_ERROR, "bad nesting of multiline comments", lex.pos)
			}
			continue
		}

		if closer > 0 {
			if closer == cur_depth {
				cur_depth = 0
			} else if closer < cur_depth {
				// ignore a lower depth
			} else {
				return NewNode(NL_ERROR, "stray #] or #]] found", lex.pos)
			}
			continue
		}

		if cur_depth == 0 {
			// a normal line, outside of any comments
			return nil
		}
	}
}

func (lex *Lexer) fetchRawLine() *Node {
	// once stream is finished, keep returning EOF
	// [ this simplifies some logic in the calling code ]
	if lex.hit_eof  {
		return NewNode(NL_EOF, "", lex.pos)
	}

	lex.pos.line += 1

	// we assume the Reader is buffered
	var buf [1]byte

	s := ""

	for {
		n, err := lex.reader.Read(buf[:])

		if n > 0 {
			if buf[0] == '\r' {
				// ignore CR
			} else if buf[0] == '\n' {
				// found the LF
				break
			} else {
				s = s + string(buf[:])
			}
		}

		if err == io.EOF {
			lex.hit_eof = true
			if s == "" {
				return NewNode(NL_EOF, "", lex.pos)
			} else {
				break
			}
		} else if err != nil {
			lex.hit_eof = true
			return NewNode(NL_ERROR, err.Error(), lex.pos)
		}
	}

	// convert to runes
	lex.line = []rune(s)
	lex.r    = lex.line

	// check for UTF-8 problems
	for _, ch := range lex.line {
		if ch == utf8.RuneError {
			return NewNode(NL_ERROR, "bad UTF-8 found", lex.pos)
		}
	}

	return nil  // ok
}

func (lex *Lexer) skipWhitespace() {
	for len(lex.r) > 0 {
		ch := lex.r[0]

		if LEX_Whitespace(ch) {
			lex.r = lex.r[1:]
		} else {
			break
		}
	}
}

func LEX_Whitespace(ch rune) bool {
	return unicode.Is(unicode.White_Space, ch) ||
		   unicode.IsControl(ch)
}

func LEX_Printable(ch rune) bool {
	return (
		unicode.IsLetter(ch) ||
		unicode.IsNumber(ch) ||
		unicode.IsSymbol(ch) ||
		unicode.IsPunct(ch) )
}

func LEX_Identifier(s string) bool {
	if s == "" {
		return false
	}
	for _, ch := range s {
		if ! LEX_IdentifierChar(ch) {
			return false
		}
	}
	return true
}

func LEX_IdentifierChar(ch rune) bool {
	const ID_CHARS = "@_.-+*/%<=>!?#$&|'~\\"

	return false ||
		unicode.IsLetter(ch) ||
		unicode.IsDigit(ch) ||
		strings.ContainsRune(ID_CHARS, ch)
}

func LEX_OctalDigit(ch rune) bool {
	return '0' <= ch && ch <= '7'
}

func LEX_HexDigit(ch rune) bool {
	ch = unicode.ToLower(ch)
	if 'a' <= ch && ch <= 'f' {
		return true
	}
	if '0' <= ch && ch <= '9' {
		return true
	}
	return false
}

func LEX_Number(r []rune) bool {
	if len(r) == 0 {
		return false
	}

	if unicode.IsDigit(r[0]) {
		return true
	}
	if len(r) < 2 {
		return false
	}
	if r[0] == '-' || r[0] == '+' {
		if unicode.IsDigit(r[1]) {
			return true
		}
	}
	return false
}

func LEX_MultilineComment(r []rune, bracket rune) int {
	if len(r) < 2 {
		return 0
	}
	if r[0] != '#' {
		return 0
	}
	count := 0
	r = r[1:]
	for len(r) > 0 {
		if r[0] != bracket {
			break
		}
		count += 1
		r = r[1:]
	}
	return count
}

func LEX_CommonDirective(s string) bool {
	switch s {
	case "type", "macro", "const", "var", "fun", "do":
		return true
	}
	return false
}

func LEX_ModuleName(s string) string {
	pos := strings.IndexRune(s, '/')
	if pos <= 0 || pos+1 >= len(s) {
		return ""
	}
	return s[0:pos]
}

func LEX_BareName(s string) string {
	m := LEX_ModuleName(s)
	if m == "" {
		return s
	}
	return s[len(m)+1:]
}

//----------------------------------------------------------------------

func NewNode(kind NodeKind, str string, pos Position) *Node {
	t := new(Node)
	t.kind = kind
	t.str  = str
	t.pos  = pos
	t.children = make([]*Node, 0)
	return t
}

func (t *Node) Len() int {
	return len(t.children)
}

func (t *Node) Last() *Node {
	if len(t.children) == 0 {
		return nil
	}
	return t.children[len(t.children)-1]
}

func (t *Node) Add(child *Node) {
	if child == nil {
		panic("Add with nil node")
	}
	t.children = append(t.children, child)
}

func (t *Node) AddFront(child *Node) {
	// resize the array
	t.children = append(t.children, nil)

	// shift elements up
	copy(t.children[1:], t.children[0:])

	t.children[0] = child
}

func (t *Node) PopHead() *Node {
	if t.Len() == 0 {
		panic("PopHead with no tokens")
	}
	head := t.children[0]
	t.children = t.children[1:]
	return head
}

func (t *Node) PopTail() *Node {
	if t.Len() == 0 {
		panic("PopTail with no tokens")
	}
	num  := len(t.children)
	tail := t.children[num-1]
	t.children = t.children[0:num-1]
	return tail
}

func (t *Node) Remove(idx int) {
	if idx >= t.Len() {
		panic("Remove with bad index")
	}
	for k := idx+1; k < t.Len(); k++ {
		t.children[k-1] = t.children[k]
	}
	t.children = t.children[0:t.Len()-1]
}

func (t *Node) Match(name string) bool {
	if t.kind == NL_Name || t.kind == NL_Symbol {
		if t.Len() == 0 {
			return t.str == name
		}
	}
	return false
}

func (t *Node) Find(name string) int {
	for i, elem := range t.children {
		if elem.Match(name) {
			return i
		}
	}
	return -1
}

func (t *Node) IsField() bool {
	if t.Match("...") || t.Match("..") {
		return false
	}
	return t.kind == NL_Name && len(t.str) >= 2 && t.str[0] == '.'
}

func (t *Node) IsMethod() bool {
	if t.kind == NL_Name {
		r := []rune(t.str)

		if r[0] == '.' {
			return false
		}

		// require last char (only) to be a hash
		if len(r) >= 2 && r[len(r)-1] == '#' {
			for i := len(r)-2; i >= 0; i-- {
				if r[i] == '#' { return false }
			}
			return true
		}
	}
	return false
}

func (t *Node) IsLiteral() bool {
	switch t.kind {
	case NL_Integer, NL_Float, NL_FltSpec, NL_String, NL_Char, NL_Bool, NL_Nil:
		return true
	}
	return false
}

func (t *Node) IsValue() bool {
	return t.val != nil
}

// String returns something usable in error messages,
// especially ones of the form "expected xxx, got: yyy".
func (t *Node) String() string {
	switch t.kind {
	case NL_Integer, NL_Float, NL_FltSpec, NL_Name, NL_Symbol:
		return t.str

	case NL_Char:   return "a char literal"
	case NL_Bool:   return "a boolean literal"
	case NL_String: return "a string literal"
	case NL_Nil:    return "NIL"

	case NG_Line:   return "a raw line"
	case NG_Expr:   return "stuff in ()"
	case NG_Data:   return "stuff in []"
	case NG_Block:  return "stuff in {}"

	case NX_Value:  return "a value"
	default:        return "something odd"
	}
}

func (t *Node) FullString() string {
	if t == nil {
		return "nil"
	}

	s := t.str
	if len(s) > 50 {
		s = s[0:50] + "..."
	}
	s = fmt.Sprintf("%q", s)

	len_str := "(" + strconv.Itoa(t.Len()) + " elem)"

	switch t.kind {
	/* low level nodes */

	case NL_ERROR:   return "ERROR " + s
	case NL_EOF:     return "EOF"
	case NL_EOLN:    return "EOLN"

	case NL_Integer: return "NL_Integer " + s
	case NL_Float:   return "NL_Float " + s
	case NL_FltSpec: return "NL_FltSpec " + s
	case NL_Char:    return "NL_Char " + s
	case NL_Bool:    return "NL_Bool " + s
	case NL_String:  return "NL_String " + s
	case NL_Nil:     return "NL_Nil"

	case NL_Name:    return "NL_Name " + s
	case NL_Symbol:  return "NL_Symbol " + s

	/* grouping nodes */

	case NG_Line:    return "NG_Line " + len_str
	case NG_Expr:    return "NG_Expr " + len_str
	case NG_Data:    return "NG_Data " + len_str
	case NG_Block:   return "NG_Block " + len_str

	/* expressions */

	case NX_Value:    return "NX_Value"
	case NX_Const:    return "NX_Const " + s
	case NX_Global:   return "NX_Global " + s
	case NX_Local:    return "NX_Local " + s

	case NX_Unary:    return "NX_Unary " + s
	case NX_Binary:   return "NX_Binary " + s
	case NX_Call:     return "NX_Call " + len_str
	case NX_Method:   return "NX_Method " + len_str
	case NX_Access:   return "NX_Access " + len_str

	case NX_Lambda:   return "NX_Lambda"
	case NX_Matches:  return "NX_Matches"
	case NX_Supports: return "NX_Supports"
	case NX_Min:      return "NX_Min"
	case NX_Max:      return "NX_Max"

	/* statements */

	case NS_Return:   return "NS_Return"
	case NS_Skip:     return "NS_Skip "  + s
	case NS_Label:    return "NS_Label " + s
	case NS_Continue: return "NS_Continue " + s
	case NS_Break:    return "NS_Break "    + s

	case NS_Let:      return "NS_Let " + s
	case NS_If:       return "NS_If"
	case NS_Loop:     return "NS_Loop"
	case NS_For:      return "NS_For"
	case NS_ForEach:  return "NS_ForEach"
	case NS_Assign:   return "NS_Assign"

	/* data constructors */

	case ND_Array:    return "ND_Array " + len_str
	case ND_Set:      return "ND_Set " + len_str
	case ND_Map:      return "ND_Map " + len_str
	case ND_MapElem:  return "ND_MapElem"
	case ND_StrBuild: return "ND_StrBuild " + len_str

	default:
		return "??UNKNOWN??"
	}
}

func Dump(msg string, t *Node, level int) {
	if msg != "" {
		println(msg)
	}

	s := fmt.Sprintf("%*s%s", level, "", t.FullString())
	println(s)

	if t.children != nil {
		for _, child := range t.children {
			Dump("", child, level + 3)
		}
	}
}
